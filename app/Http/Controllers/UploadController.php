<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class UploadController extends Controller
{

    public function __construct(){

        parent::__construct();

        $this->s3url=awsurl();

        $this->middleware('auth');
    }


    public function newUpload(Request $request) {

        $inputs = $request->all();

        $type = $request->query('type');

        $v = $this->validator($inputs);

        if ($v->fails()) {
            return array('status' => 'Error', 'errors' => $v->errors()->first());
        }

        if($request->hasFile('file')) {
            $file = $request->file('file');

            $getMimeType=$file->getMimeType();
            $getlk = $this->image_check($getMimeType, $file);

            if($getlk === false){
                return array('status' => 'Error', 'errors' => 'No valid images');
            }

            try{

                $img = Image::make($file);
                $ImageMimeType = $img->mime();

                $getl = $this->image_check($ImageMimeType, $file);
                if($getl === false){
                    return array('status' => 'Error', 'errors' => 'No valid images');
                }


            }catch(\Exception $e){
                Log::info('Image::make:error'.$file);
                return array('status' => 'Error', 'errors' => 'No valid images');
            }





            $tmpUserName = Auth::user()->id .'-' ;

            $tmpTimeToken = time(); //for if same image was uploaded.

            $tmpFileName = $tmpTimeToken.'-'.$file->getClientOriginalName();

            $tmpFilePath = 'upload/tmp/';

            $hardPath =   date('Y-m') .'/'.date('d') .'/';


            if (!file_exists(public_path() .'/'.$tmpFilePath.$hardPath )) {
                $oldmask = umask(0);
                mkdir(public_path() .'/'. $tmpFilePath.$hardPath , 0777, true);
                umask($oldmask);
            }



            if($type!='preview' and $type!='answer' and $getMimeType=='image/gif'){

                $file->move(public_path() . '/'. $tmpFilePath. $hardPath, $tmpUserName.$tmpFileName);

                $path = $tmpFilePath .$hardPath . $tmpUserName. $tmpFileName;

                $patha='/'.$path;

            }else{

                    if($type=='entry'){

                        $img->resize(config('buzzytheme_'.getenvcong('CurrentTheme').'.entry-image_big_width'), null, function ($constraint) {
                         $constraint->aspectRatio();
                         $constraint->upsize();
                        });

                    }else if($type=='preview'){

                        $img->fit(config('buzzytheme_'.getenvcong('CurrentTheme').'.preview-image_big_width'), config('buzzytheme_'.getenvcong('CurrentTheme').'.preview-image_big_height'));


                    }else if($type=='answer'){

                        $img->fit(240, 240);

                    }


                $path = $tmpFilePath .$hardPath . $tmpUserName. md5($tmpFileName). '.jpg';

                $imgo= $img->save($path);

                $patha='/'.$path;


                if(env('APP_FILESYSTEM')=="s3"){

                    \Storage::disk('s3')->put($path, $imgo->stream()->__toString());

                    \File::delete(public_path($path));

                    $patha= $this->s3url.$path;
                }
            }


            if(	strpos($patha,'.php') or strpos($patha,'.asp') or strpos($patha,'.txt') ){

                try{
                    Log::info('deleting  '.$patha);
                    \File::delete(public_path($path));
                    Log::info('deleting 1 '.$patha);
                    unlink($path);
                    Log::info('deleting 2 '.$patha);
                    unlink($patha);
                    Log::info('deleted '.$patha);

                }catch(\Exception $e){
                    Log::info('error while deleting '.$patha);
                }
                return response()->json(array('error'=> 'No valid images'),  200);
            }

            return response()->json(array('path'=> $patha), 200);
        } else {
            return response()->json(array('error'=> 'Pick a image'),  200);
        }

    }



    /**
     * Validator of question posts
     *
     * @param $inputs
     * @return array|bool
     */
    protected function validator(array $inputs)
    {

        $rules = [
            'file' => 'required|mimes:jpg,jpeg,gif,png',
        ];

        return \Validator::make($inputs, $rules);
    }


    public function mime($type)
    {
        if ($type == 'image/jpeg' ||
            $type == 'image/jpg' ||
            $type == 'image/gif' ||
            $type == 'image/png') {
            return 'image';
        }
        return null;
    }

    public function image_check($getMimeType, $file)
    {

        if ($this->mime($getMimeType) != 'image') {
            Log::info('image_check:mime-1- '.$file->getClientOriginalName());
            Auth::logout();
            return false;
        }

        if(	strpos($file->getClientOriginalName(),'.php')  || strpos($file->getClientOriginalName(),'.txt')  || strpos($file->getClientOriginalName(),'.asp') ){
            Log::info('Uimage_check:getClientOriginalName:uploading-- '.$file->getClientOriginalName());
            Auth::logout();
            return false;
        }

        return true;
    }
}
