<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Create Page Lines
    |--------------------------------------------------------------------------
    */
    'create'            => 'Buat :type',
    'title'             => 'Judul',
    'titleplace'        => 'Tuliskan Judul',
    'desc'              => 'Deskripsi',
    'descplace'         => 'Tuliskan Deskripsi',
    'entries'           => ':type Entries',

    'listtype'          => 'List Tipe',
    'listasc'           => 'Numeric Asc List',
    'listdesc'          => 'Numeric Desc List',
    'normallist'        => 'Normal List',


    'option'            => 'Poll',
    'text'              => 'Text',
    'image'             => 'Image',
    'video'             => 'Video',
    'embed'             => 'Iframe',

    'add'               => 'Add :type',
    'edit'               => 'Edit :type',

    'mored'             => 'More Details',
    'lessd'             => 'Less Details',

    'entry_title'       => 'Title',
    'entry_titleop'     => 'Title (optional)',
    'entry_body'        => 'Add some text about this entry.',
    'entry_source'      => 'Source (optional)',
    'entry_embeddesc'   => 'Enter embed or iframe',
    'entry_addimage'    => 'Click to add a photo.',

    'preview'           => 'Preview Image',
    'pickpreview'       => 'Pick a preview image.',
    'categories'        => 'Categories',

    'makepreview'       => 'Make preview image',


    'addnew'            => 'Add New Entry',
    'savec'             => 'Save Changes',
    'createp'           => 'Create',
    'cancel'            => 'Cancel',

    'videotips'            => 'Please paste url to above input. Supported sites: <b>youtube</b>, <b>facebook</b>, <b>dailymotion</b>, <b>vimeo</b>',

];
